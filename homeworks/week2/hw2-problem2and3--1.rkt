#lang typed/racket/no-check

(require (rename-in
          2htdp/image
          [square draw-square]
          [circle draw-circle]))

(struct shape1 ([border-square : square]
                [inner-circle : circle]
                [inner-square : square]) #:transparent)

(struct shape2 ([outer-shape : shape1]
                [inner-shape : shape1]) #:transparent)

(struct shape3 ([outer-shape : shape1]
                [inner-shape : shape2]) #:transparent)

(require "../../utilities/index.rkt")

(: inner-circle-radius (Number -> Number))
(define (inner-circle-radius border-side)
  (half border-side))

(: inner-square-side (Number -> Number))
(define (inner-square-side border-side)
  (hypotenuse-eq (half border-side)))

(: mk-shape1 (Number -> shape1))
(define (mk-shape1 border-side)
  (shape1 (square border-side)
          (circle (inner-circle-radius border-side))
          (square (inner-square-side border-side))))

(: mk-shape2 (Number -> shape2))
(define (mk-shape2 border-side)
  (shape2 (mk-shape1 border-side)
          (mk-shape1 (inner-square-side border-side))))

(: mk-shape3 (Number -> shape3))
(define (mk-shape3 border-side)
  (shape3 (mk-shape1 border-side)
          (mk-shape2 (inner-square-side border-side))))

(: tilt (shape -> image))
(define (tilt sh) (rotate 45 sh))

; Find area
(: area-visible-circle-parts1 (shape1 -> Number))
(define (area-visible-circle-parts1 sh)
  (let* ([inner-circle (shape1-inner-circle sh)]
         [inner-square (shape1-inner-square sh)])
    (- (area-circle inner-circle)
       (area-square inner-square)
    )
  )
)

(: area-visible-circle-parts2 (shape2 -> Number))
(define (area-visible-circle-parts2 sh)
  (let* ([outer-shape (shape2-outer-shape sh)]
         [inner-shape (shape2-inner-shape sh)])
    (+ (area-visible-circle-parts1 outer-shape)
       (area-visible-circle-parts1 inner-shape))))

(: area-visible-circle-parts3 (shape2 -> Number))
(define (area-visible-circle-parts3 sh)
  (let* ([outer-shape (shape3-outer-shape sh)]
         [inner-shape (shape3-inner-shape sh)])
    (+ (area-visible-circle-parts1 outer-shape)
       (area-visible-circle-parts2 inner-shape))))

; Draw
(require (rename-in
          2htdp/image
          [circle draw-circle]
          [square draw-square]))

(: red-circle (circle -> Void))
(define (red-circle c)
  (draw-circle (circle-radius c) 'solid 'red))

(: white-square (square -> image))
(define (white-square sq)
  (let* ([sq-side (square-side sq)])
    (overlay (draw-square (square-side sq) 'solid 'white)
             (draw-square (square-side sq) 'outline 'black))))

(: draw-shape1 (shape1 -> image))
(define (draw-shape1 sh)
  (let* ([outer-square (shape1-border-square sh)]
         [inner-circle (shape1-inner-circle sh)]
         [inner-square (shape1-inner-square sh)])
    (overlay (tilt (white-square inner-square))
             (red-circle inner-circle)
             (white-square outer-square))))

(: draw-shape2 (shape2 -> image))
(define (draw-shape2 sh)
  (let* ([outer-shape (shape2-outer-shape sh)]
         [inner-shape (shape2-inner-shape sh)])
    (overlay (tilt (draw-shape1 inner-shape))
             (draw-shape1 outer-shape))))

(: draw-shape3 (shape3 -> image))
(define (draw-shape3 sh)
  (let* ([outer-shape (shape3-outer-shape sh)]
         [inner-shape (shape3-inner-shape sh)])
    (overlay (tilt (draw-shape2 inner-shape))
             (draw-shape1 outer-shape))))

; Test data
(define sh1-100 (mk-shape1 100))
(area-visible-circle-parts1 sh1-100)
(draw-shape1 sh1-100)

(define sh2-100 (mk-shape2 100))
(area-visible-circle-parts2 sh2-100)
(draw-shape2 sh2-100)

(define sh3-100 (mk-shape3 100))
(define sh3-200 (mk-shape3 200))
;(area-visible-circle-parts3 sh3-100)
;(draw-shape3 sh3-100)
(area-visible-circle-parts3 sh3-200)
(draw-shape3 sh3-200)
