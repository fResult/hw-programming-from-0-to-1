# Problem 1 - Draw anything that surprises the teacher
## Thinking About the Parts
![Robot Parts](https://github.com/fResult/homework-programming-from-0-to-1/assets/19329932/bd8c9231-cdbb-4ffe-94ae-6b30bc6eb8cf)

## Extracting the Sub-problems
![Robot Sub-problems](https://github.com/fResult/homework-programming-from-0-to-1/assets/19329932/873a10f4-8ada-4571-a465-61a41628e4c9)

## Result After Implementing
**Implementation is here:**<br/>
https://github.com/fResult/homework-programming-from-0-to-1/blob/main/homeworks/week2/hw2-problem1.rkt

![Result by robot's height 300](https://github.com/fResult/homework-programming-from-0-to-1/assets/19329932/6ce756df-4d2f-444f-960c-2940cca680c6)
![Result by robot's height 500](https://github.com/fResult/homework-programming-from-0-to-1/assets/19329932/1e52c71c-245b-4adc-a79d-c699d3b34dce)
